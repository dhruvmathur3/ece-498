import math
import time


import rospy
from gazebo_msgs.msg import  ModelState
from controller import bicycleModel

import matplotlib.pyplot as plt


if __name__ == "__main__":
    rospy.init_node("model_dynamics")

    model = bicycleModel()

    endList = 0;
    x_list = []
    y_list = []
    waypoint_x = []
    waypoint_y = []

    #wait till a waypoint is received
    while(not model.waypointList):
        pass
        rospy.sleep(7)

    targetState = ModelState()
    targetState = model.waypointList.pop(0)


    start = time.time()

    rate = rospy.Rate(100)  # 100 Hz
    while not rospy.is_shutdown():
        rate.sleep()  # Wait a while before trying to get a new state

        currState =  model.getModelState()
        if not currState.success:
            continue


        distToTargetX = abs(targetState.pose.position.x - currState.pose.position.x)
        distToTargetY = abs(targetState.pose.position.y - currState.pose.position.y)
        x_list.append(currState.pose.position.x)
        y_list.append(currState.pose.position.y)
        waypoint_x.append(targetState.pose.position.x)
        waypoint_y.append(targetState.pose.position.y)

        if(distToTargetX < 1 and distToTargetY < 1):
            if not model.waypointList:
                newState = ModelState()
                newState.model_name = 'polaris'
                newState.pose = currState.pose
                newState.twist.linear.x = 0
                newState.twist.linear.y = 0
                newState.twist.angular.z = 0
                model.modelStatePub.publish(newState)
                #only print time the first time waypontList is empty
                if(not endList):
                    endList = 1
                    end = time.time()
                    print("Time taken:", end-start)
                    plt.plot(waypoint_x, waypoint_y, 'k--')
                    # plt.scatter(waypoint_x, waypoint_y, 'g')
                    plt.plot(x_list, y_list, 'r')
                    plt.show()
            else:
                if(endList):
                    start = time.time()
                    endList = 0
                targetState = model.waypointList.pop(0)
                markerState = ModelState()
                markerState.model_name = 'marker'
                markerState.pose = targetState.pose
                model.modelStatePub.publish(markerState)
        else:
            model.setModelState(currState, targetState)
            # print(distToTargetX,distToTargetY)
            markerState = ModelState()
            markerState.model_name = 'marker'
            markerState.pose = targetState.pose
            model.modelStatePub.publish(markerState)


    
    # rospy.spin()