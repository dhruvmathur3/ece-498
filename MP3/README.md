# Python Robotics

PythonRobotics is a library created by Atsushi Sakai that has a lot of robotics demos. I added his AStar and HybridAStar files so we can get a better idea of how they work. We should play around with the files so we can figure out how to apply them to MP3.

## Requirements

- Python 3.7.x (2.7 is not supported)

- numpy

- scipy

- matplotlib

- pandas

- [cvxpy](https://www.cvxpy.org/index.html) 

## Installation

To install the libraries required for this project, I would create a virtual environment outside of the git repository (so that the libraries in the user environment don't get messed up).

To create a virtual environment, run the following:

```
python3 -m venv my-env
```

To activate this environment, run the following:
```
source my-env/bin/activate
```

To install the necessary requirements, run the following:
```
pip install numpy
pip install scipy
pip install matplotlib
pip install pandas
pip install cvxpy
```

## References

To reference the PythonRobotics library, use this bibtex command:
```
@article{sakai2018pythonrobotics,
  title={Pythonrobotics: a python code collection of robotics algorithms},
  author={Sakai, Atsushi and Ingram, Daniel and Dinius, Joseph and Chawla, Karan and Raffin, Antonin and Paques, Alexis},
  journal={arXiv preprint arXiv:1808.10703},
  year={2018}
}
```