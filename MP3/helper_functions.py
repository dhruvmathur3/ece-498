import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np
import priority_queue

# Possible Steering Angles
delta = [-50,-10,0,10,50]

min_x = -6
max_x = 6
min_y = -6
max_y = 6

goal_Coord = [5,5]

start_Coord = [-5,-5]

chord_length = 1.43

class Node:
  def __init__(self, disc_x, disc_y):
    self.node_Name = [disc_x, disc_y]
    # initial heading angle
        # dist from start
        # dist to goal
        # path via
        # comb heuristic
        # cont coortd
        # angle used to access

###############################################################################
# Returns a list of possible reached discrete cells and the required steering angle
# [[x,y], steering angle]
###############################################################################
def possible_chord_paths(cont_x, cont_y, theta):

    # Forwards options
    forward_options = []

    for steering_angle in delta:

        if steering_angle == -50:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 50))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 50))),2)

        elif steering_angle == -10:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 10))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 10))),2)

        elif steering_angle == 0:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta))),2)

        elif steering_angle == 10:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 10))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 10))),2)

        elif steering_angle == 50:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 50))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 50))),2)

        disc_cell = find_discrete_cell(new_x, new_y)
        if (disc_cell[0] <= max_x and disc_cell[0] >= min_x) and (disc_cell[1] <= max_y and disc_cell[1] >= min_y):
            forward_options.append([disc_cell, steering_angle])

    return forward_options

###############################################################################
# Finds which discrete node corresponds to the cont. coords
###############################################################################
def find_discrete_cell(cont_x, cont_y):

    return [round(cont_x),round(cont_y)]


###############################################################################
# Finds new state using equations from MP docs.
###############################################################################
def new_state(old_x, old_y, old_theta, velocity, steer_angle):

    new_Theta = round(old_theta + math.degrees(0.5 * math.tan(math.radians(steer_angle))))
    
    new_x = round(old_x + (velocity * math.cos(math.radians(new_Theta))),2)
    new_y = round(old_y + (velocity * math.sin(math.radians(new_Theta))),2)

    return new_x, new_y, new_Theta

###############################################################################
# Find cost to goal (euclidean dist)
###############################################################################
def cost_to_goal(disc_x, disc_y):

    cost = round((goal_Coord[0] - disc_x)**2 + (goal_Coord[1] - disc_y)**2,2)

    return cost

###############################################################################
# Find cost to goal (euclidean dist)
###############################################################################
def dist_from_start(disc_x, disc_y):

    cost = round((start_Coord[0] - disc_x)**2 + (start_Coord[1] - disc_y)**2,2)

    return cost

###############################################################################
# Checks if cell is NN - use discrete coords
###############################################################################
def can_visit(orig_x, orig_y, next_x, next_y):

    if math.sqrt((next_x - orig_x)**2 + (next_y - orig_y)**2) > math.sqrt(2):
        return False

    elif ((orig_x == next_x) and (orig_y == next_y)):
        return False 
    
    return True



#--------------------------------------------------------------------------------------#
# For testing
#options = possible_chord_paths(-3, -5, 0)
#print(options)

#print(NN_check(-5,-5,-4,-3))
#print(cost_to_goal(-2,-6))

#print(dist_from_start(-2,-6))

#print(new_state(-4,-5,0,1,0))