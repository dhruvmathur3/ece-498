"""
Written by Tianqi Liu, 2020 Feb.

It finds the optimal path for a car using Hybrid A* and bicycle model. 
"""

import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np

import helper_classes
import helper_functions
import heapq
import itertools
import operator


#possible steering controls
possible_str = {
    'l': -10,
    'l+': -50,
    'r+': +50,
    'r': +10,
    's': 0
}

#possible speed controls
possible_sp = {
    'f': 1,
    'b': -1
}

#----------------------------------------------------------------------------------#

# total cost f(n) = actual cost g(n) + heuristic cost h(n)
class hybrid_a_star:
    def __init__(self, min_x, max_x, min_y, max_y, \
            obstacle=[], resolution=1, vehicle_length=2):
        ##TODO

        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.obstacles = {}
        self.obstacle = obstacle
        for obs in obstacle:
            self.obstacles[obs] = 'yes' # make obstacles into a dictionary
        self.resolution = resolution
        self.vehicle_length = vehicle_length

        self.rejected_nodes = {}

        self.visited_nodes = []

        self.available_nodes = {}

        ###


    """
    For each node n, we need to store:
    (discret_x, discret_y, heading angle theta), 
    (continuous x, continuous y, heading angle theta)
    cost g, f,
    path [(continuous x, continuous y, continuous theta),...]

    start: discret (x, y, theta)
    end: discret (x, y, theta)
    sol_path = [(x1,y1,theta1),(x2,y2,theta2), ...]
    """
    def find_path(self, start, end):
        sol_path = []

        priority_queue = helper_classes.Priority_Queue()

        ##TODO
        start_x = start[0]
        start_y = start[1]
        start_heading = start[2]
        # set up initial starting node
        curr_node = helper_classes.Node(start_x,start_y)
        curr_node.heading = start_heading
        curr_node.steering_angle_used = 0

        # find possible nodes to go to
        options = helper_functions.possible_chord_paths(start_x,start_y,start_heading)

        # which nodes can be visited from the current coords
        for i in range(len(options)):

            x = options[i][0][0]
            y = options[i][0][1]
            steering = options[i][1]

            if (not self.check_if_obs(x,y) and ((x,y,steering) not in self.rejected_nodes)):
                curr_node.can_visit[(x,y,steering)] = 'yes'
            

        # insert options into pqueue
        self.add_options_to_pqueue(options, start_x,start_y, priority_queue,-5, -5, 0)

        # insert starting node into path
        sol_path.append(start)
    
        # mark node as visited
        self.visited_nodes.append(curr_node) 

        #update cont coords on current node
        cont_coords = start
        curr_node.cont_coords = cont_coords

        while len(priority_queue.pq) > 0:

            queue_buffer = []
            # check if next node can be explored
            next_node_id = priority_queue.pop_task()
            
            while next_node_id not in curr_node.can_visit:
                
                queue_buffer.append(next_node_id)
                
                # refill queue to make sure that all nodes in buffer
                if len(priority_queue.pq) == 0 and len(queue_buffer) > 0:
                    while len(queue_buffer) > 0:
                        priority = self.available_nodes[queue_buffer[len(queue_buffer)-1]].combined_heuristic
                        priority_queue.add_task(queue_buffer.pop(), priority)

                next_node_id = priority_queue.pop_task()

                if len(priority_queue.pq) == 0 and (next_node_id not in curr_node.can_visit):
                    sol_path.pop()
                    self.visited_nodes.pop()
                    curr_node = self.visited_nodes[-1]
                    cont_coords = curr_node.cont_coords

                    # put nodes back from buffer
                    # put all popped nodes back in Pqueue
                    while len(queue_buffer) > 0:
                        priority = self.available_nodes[queue_buffer[len(queue_buffer)-1]].combined_heuristic
                        priority_queue.add_task(queue_buffer.pop(), priority) 
                    continue


            curr_node.can_visit[next_node_id] = 'no'

             # explore next node
            curr_node = self.available_nodes[next_node_id]

            # mark node as visited
            self.visited_nodes.append(curr_node) 

            del self.available_nodes[next_node_id]

            if len(self.available_nodes) == 0:
                print('No path available')
                sol_path.clear()
                # dummy value so that something prints
                return sol_path

            #update cont coords on current node
            cont_coords = helper_functions.new_state(cont_coords[0],cont_coords[1],cont_coords[2],1,curr_node.steering_angle_used)
            curr_node.cont_coords = cont_coords
            # add to path
            sol_path.append(cont_coords)


            if (cont_coords[0] < self.min_x) or (cont_coords[0] > self.max_x) or (cont_coords[1] > self.max_y) or (cont_coords[1] < self.min_y):
                sol_path.pop()
                self.visited_nodes.pop()
                curr_node = self.visited_nodes[-1]
                cont_coords = curr_node.cont_coords

                # put nodes back from buffer
                # put all popped nodes back in Pqueue
                while len(queue_buffer) > 0:
                    priority = self.available_nodes[queue_buffer[len(queue_buffer)-1]].combined_heuristic
                    priority_queue.add_task(queue_buffer.pop(), priority) 

                continue

            if (round(cont_coords[0]),round(cont_coords[1])) in self.obstacles:
                sol_path.pop()
                self.visited_nodes.pop()
                curr_node = self.visited_nodes[-1]
                cont_coords = curr_node.cont_coords

                # put nodes back from buffer
                # put all popped nodes back in Pqueue
                while len(queue_buffer) > 0:
                    priority = self.available_nodes[queue_buffer[len(queue_buffer)-1]].combined_heuristic
                    priority_queue.add_task(queue_buffer.pop(), priority) 

                continue


            # reached the end
            if (curr_node.node_Name[0],curr_node.node_Name[1]) == (end[0],end[1]):
                break

            start_x = cont_coords[0]
            start_y = cont_coords[1]
            heading = cont_coords[2]

            # find possible nodes to go to
            options = helper_functions.possible_chord_paths(start_x,start_y,heading)

            # which nodes can be visited from the current coords
            for i in range(len(options)):

                x = options[i][0][0]
                y = options[i][0][1]
                steering = options[i][1]

                if (not self.check_if_obs(x,y) and ((x,y,steering) not in self.rejected_nodes)):
                    curr_node.can_visit[(x,y,steering)] = 'yes'

            # insert options into pqueue
            self.add_options_to_pqueue(options, curr_node.node_Name[0], curr_node.node_Name[1], priority_queue, curr_node.path_via[0], curr_node.path_via[1], curr_node.dist_from_start)

            # put nodes back from buffer
             # put all popped nodes back in Pqueue
            while len(queue_buffer) > 0:
                priority = self.available_nodes[queue_buffer[len(queue_buffer)-1]].combined_heuristic
                priority_queue.add_task(queue_buffer.pop(), priority) 

            # if no options have to go back to previous node
            if len(options) == 0:
                sol_path.pop()
                self.visited_nodes.pop()
                curr_node = self.visited_nodes[-1]
                cont_coords = curr_node.cont_coords
                continue

        ###

        return sol_path 

    ###############################################################################
    # Adds nodes to Pqueue 
    ###############################################################################   
    def add_options_to_pqueue(self, options, start_x, start_y, priority_queue, from_x, from_y, prev_dist_from_start):

        for i in range(len(options)):

            disc_x = options[i][0][0]
            disc_y = options[i][0][1]

            # check if node is obstacle
            if self.check_if_obs(disc_x, disc_y):
                continue

            steering_angle = options[i][1]
            node = helper_classes.Node(disc_x, disc_y)
            # node.dist_from_start = abs(steering_angle / 5) + helper_functions.dist_from_start(disc_x, disc_y)
            
            node.dist_from_start = abs(steering_angle / 15) + prev_dist_from_start + ((from_x - start_x)**2 + (from_y - start_y)**2)
            
            node.dist_to_goal = helper_functions.cost_to_goal(disc_x, disc_y)
            node.path_via = (start_x,start_y, steering_angle)
            #node.combined_heuristic = node.dist_from_start + node.dist_to_goal
            node.combined_heuristic = node.dist_from_start + node.dist_to_goal
            node.steering_angle_used = steering_angle
            # print("Node Details:")
            # print(node.node_Name)
            # print(node.dist_from_start)
            # print(node.dist_to_goal)
            # print(node.path_via)
            # print(node.combined_heuristic)
            # print(node.steering_angle_used)
            self.available_nodes[(disc_x,disc_y,steering_angle)] = node
            priority_queue.add_task((disc_x,disc_y,steering_angle), node.combined_heuristic)

    ###############################################################################
    # Check if obstacle
    ###############################################################################  
    def check_if_obs(self, disc_x, disc_y):

        coords = (disc_x,disc_y)
        
        if coords in self.obstacles:
            return True
        elif (disc_x < self.min_x) or (disc_x > self.max_x) or (disc_y < self.min_y) or (disc_y > self.max_y):
            return True
        else:
            return False

#----------------------------------------------------------------------------------#

def main():
    print(__file__ + " start!!")

    # start and goal position
    #(x, y, theta) in meters, meters, degrees 
    sx, sy, stheta= -5, -5, 0
    gx, gy, gtheta = 5, 5, 0
    # sx, sy, stheta= -5, -5, 0
    # gx, gy, gtheta = 0, 0, 0

    #create obstacles
    obstacle = []

    for i in range(2):
        obstacle.append((0,i))
        obstacle.append((0,-i))  

    # obstacle.append((-2,-5))
    # obstacle.append((-2,-4))
    # obstacle.append((-2,-3))
    # obstacle.append((-2,-2))
    # obstacle.append((-3,-3))
    # obstacle.append((-3,-3))
    # obstacle.append((-3,-4))
    # obstacle.append((-3,-5))
    # obstacle.append((-3,-6))
    # obstacle.append((-1,-1))
    # obstacle.append((-2,-1))
    # obstacle.append((-3,-1))
    # obstacle.append((-4,-1))
    # obstacle.append((-0,-1))
    # obstacle.append((-0,-2))
    # obstacle.append((-0,-3))
    # obstacle.append((-0,-4))
    # obstacle.append((-0,-5))
    # obstacle.append((-5,-1))


    ox, oy = [], []
    for (x,y) in obstacle:
        ox.append(x)
        oy.append(y)

    plt.plot(ox, oy, ".k")
    plt.plot(sx, sy, "xr")
    plt.plot(gx, gy, "xb")
    plt.grid(True)
    plt.axis("equal")

    hy_a_star = hybrid_a_star(-6, 6, -6, 6, obstacle=obstacle, \
        resolution=1, vehicle_length=2)
    path = hy_a_star.find_path((sx,sy,stheta), (gx,gy,gtheta))

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    plt.plot(rx, ry, "-r")
    plt.show()


if __name__ == '__main__':
    main()