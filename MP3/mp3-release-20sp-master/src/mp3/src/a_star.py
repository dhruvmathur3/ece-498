import copy
from heapq import heappush, heappop
import math
import matplotlib.pyplot as plt
import numpy as np


# car state = (x,y)
# state tuple (f,g,(x,y), [(x1,y1),(x2,y2)...])
# total cost f(n) = actual cost g(n) + heuristic cost h(n)
# obstacles = [(x,y), ...]
# min_x, max_x, min_y, max_y are the boundaries of the environment
class a_star:
    def __init__(self, min_x, max_x, min_y, max_y, \
            obstacle=[], resolution=1, robot_size=1):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.res = resolution
        self.obs = obstacle
        self.bad_nodes = []
        self.size = robot_size

    # state: (total cost f, previous cost g, current position (x,y), \
    # previous motion id, path[(x1,y1),...])
    # start = np.array([sx, sy])
    # end = np.array([gx, gy])
    # sol_path = [(x1,y1),(x2,y2), ...]

    # function to calculate heuristic
    def calc_heuristic(self, pos, end):
        x, y = pos
        gx, gy = end
        return math.hypot(x - gx, y - gy)

    # function to store paths in Q: (f, g, [path])
    def new_path(self, P, g, start, end, Q):
        head = P[-1]
        x, y = head
        res = self.res
        # print(g)

        new_nodes = [(x-res, y-res), (x-res, y), (x-res, y+res),
                     (x, y+res), (x, y-res),
                     (x+res, y-res), (x+res, y), (x+res, y+res)]

        for node in new_nodes:
            xn, yn = node
            if not (self.min_x <= xn <= self.max_x and self.min_y <= yn <= self.max_y and (xn, yn) not in self.obs):
                self.bad_nodes.append(node)
            elif node not in self.bad_nodes:
                h = self.calc_heuristic(node, end)
                # g = self.calc_heuristic(node, start)
                f = h+g
                g_new = g + self.res
                path = P+[node]
                heappush(Q, (h, g_new, path))
            self.bad_nodes.append(node)



    def find_path(self, start, end):
        sol_path = []

        ##TODO
        Q = []
        # Add start and heuristic to Q
        h_s = self.calc_heuristic(start, end)
        g_s = 0
        heappush(Q, (g_s+h_s, g_s, [start]))

        while Q != []:
            f, g, P = heappop(Q)
            if P[-1] == end:
                sol_path = P
                break
            else:
                self.new_path(P, g, start, end, Q)
                self.bad_nodes.append(P[-1])

        return sol_path


def main():
    print(__file__ + " start!!")

    grid_size = 1  # [m]
    robot_size = 1.0  # [m]

    sx, sy = -10, -10
    gx, gy = 10, 10
    obstacle = []
    for i in range(30):
        obstacle.append((i-15, -15))
        obstacle.append((i-14, 15))
        obstacle.append((-15, i-14))
        obstacle.append((15, i-15))

    for i in range(3):
        obstacle.append((0,i))
        obstacle.append((0,-i))

    plt.plot(sx, sy, "xr")
    plt.plot(gx, gy, "xb")
    for obs in obstacle:
        plt.plot(obs[0], obs[1], "xk")
    plt.grid(True)
    plt.axis("equal")

    simple_a_star = a_star(-15, 15, -15, 15, obstacle=obstacle, \
        resolution=grid_size, robot_size=robot_size)
    path = simple_a_star.find_path((sx,sy), (gx,gy))
    print (path)

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    plt.plot(rx, ry, "-r")
    plt.show()


if __name__ == '__main__':
    main()
