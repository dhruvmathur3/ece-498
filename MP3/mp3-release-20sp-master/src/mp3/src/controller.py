import math
import rospy
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.msg import ModelState
from ackermann_msgs.msg import AckermannDrive


class bicycleModel():

    def __init__(self):

        self.length = 1.88
        pt1 = ModelState()
        pt2 = ModelState()
        pt3 = ModelState()

        pt1.pose.position.x = -10
        pt1.pose.position.y = -10
        pt1.twist.linear.x = .25
        pt1.twist.linear.y = .25
        #pt1.twist.angular = 0

        pt2.pose.position.x = 10
        pt2.pose.position.y = 10
        pt2.twist.linear.x = .25
        pt2.twist.linear.y = .25
        #pt2.twist.angular = 0

        pt3.pose.position.x = 0
        pt3.pose.position.y = 0
        pt1.twist.linear.x = .25
        pt1.twist.linear.y = .25
        #pt3.twist.angular = 0

        self.waypointList = []

        self.waypointSub = rospy.Subscriber("/gem/waypoint", ModelState, self.__waypointHandler, queue_size=1)
        self.waypointPub = rospy.Publisher("/gem/waypoint", ModelState, queue_size=1)

        self.modelStatePub = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=1)

    def getModelState(self):
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            serviceResponse = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            modelState = serviceResponse(model_name='polaris')
        except rospy.ServiceException as exc:
            rospy.loginfo("Service did not process request: "+str(exc))
        return modelState

    def rearWheelModel(self, ackermannCmd):
            """
                Description:
                    Contains the mathematical model that will represent the vehicle in gazebo
    
                Inputs:
                    ackermannCmd (AckermannDrive): contains desired vehicle velocity and steering angle velocity
                                                   that the model should follow
    
                Returns:
                    A List containing the vehicle's x velocity, y velocity, and steering angle velocity
            """
            currentModelState = self.getModelState()
    
            if not currentModelState.success:
                return
    
            ## TODO: Compute Bicyle Model
            cur_x = currentModelState.pose.orientation.x
            cur_y = currentModelState.pose.orientation.y
            cur_z = currentModelState.pose.orientation.z
            cur_w = currentModelState.pose.orientation.w
    
            cur_roll, cur_pitch, cur_yaw = self.quaternion_to_euler(cur_x, cur_y, cur_z, cur_w)
            # cur_yaw = self.quaternion_to_euler(cur_x, cur_y, cur_z, cur_w)
    
            v_r = ackermannCmd.speed
            # w = ackermannCmd.steering_angle_velocity
            w = ackermannCmd.steering_angle
    
            x_r_dot = v_r * math.cos(cur_yaw)
            y_r_dot = v_r * math.sin(cur_yaw)
            theta_dot = w
            
    
            return [x_r_dot, y_r_dot, theta_dot]    ## TODO: Return x velocity, y velocity, and steering angle velocity


    def rearWheelFeedback(self, currentState, targetState):
            """
                Description:
                    Feedback loop which drives the vehicles to the current waypoint
    
                Inputs:
                    currentState (ModelState): The curret state of the vehicle in gazebo
                    targetState  (ModelState): The desired target state of the vehicle in gazebo
    
                Returns:
                    ackermannCmd (AckermannDrive): Will be used to compute the new x,y, and steering angle
                                                   velocities of the model
            """
    
            targetVel = math.sqrt((targetState.twist.linear.x*targetState.twist.linear.x) + ((targetState.twist.linear.y*targetState.twist.linear.y)))
            targetAngVel = targetState.twist.angular.z
    
            cur_x = currentState.pose.orientation.x
            cur_y = currentState.pose.orientation.y
            cur_z = currentState.pose.orientation.z
            cur_w = currentState.pose.orientation.w
    
            cur_roll, cur_pitch, cur_yaw = self.quaternion_to_euler(cur_x, cur_y, cur_z, cur_w)
    
            tar_x = targetState.pose.orientation.x
            tar_y = targetState.pose.orientation.y
            tar_z = targetState.pose.orientation.z
            tar_w = targetState.pose.orientation.w
    
            tar_roll, tar_pitch, tar_yaw = self.quaternion_to_euler(tar_x, tar_y, tar_z, tar_w)
    
            ## TODO: Compute Error to current waypoint
            #x_e = (math.cos(cur_yaw) * (tar_x-cur_x)) + (math.sin(cur_yaw) * (tar_y-cur_y))
            #y_e = (-1*math.sin(cur_yaw) * (tar_x-cur_x)) + (math.cos(cur_yaw) * (tar_y-cur_y))
    
            # Try this insatead
            x_e = math.cos(cur_yaw)*(targetState.pose.position.x - currentState.pose.position.x) + math.sin(cur_yaw)*(targetState.pose.position.y - currentState.pose.position.y)
            y_e = -math.sin(cur_yaw)*(targetState.pose.position.x - currentState.pose.position.x) + math.cos(cur_yaw)*(targetState.pose.position.y - currentState.pose.position.y)
            tar_yaw = self.quaternion_to_euler(targetState.pose.orientation.x, targetState.pose.orientation.y, targetState.pose.orientation.z, targetState.pose.orientation.w)[2]
    
    
            theta_e = tar_yaw - cur_yaw
    
            ## TODO: Create new AckermannDrive message to return
            k_1 = 4
            k_2 = 15
            k_3 = 4
            v_r = (targetVel * math.cos(theta_e)) + (k_1 * x_e)
            w = targetAngVel + (targetVel * (k_2*y_e + k_3*math.sin(theta_e)))
    
            new_Ackermann = AckermannDrive()
            new_Ackermann.steering_angle = w
            # new_Ackermann.steering_angle_velocity = w
            #
            new_Ackermann.speed = v_r
    
            return new_Ackermann


    def setModelState(self, currState, targetState):

        #control = AckermannDrive()
        control = self.rearWheelFeedback(currState, targetState)
        values = self.rearWheelModel(control)

        newState = ModelState()
        newState.model_name = 'polaris'
        newState.pose = currState.pose
        newState.twist.linear.x = values[0]
        newState.twist.linear.y = values[1]
        newState.twist.angular.z = values[2]
        self.modelStatePub.publish(newState)

    def quaternion_to_euler(self, x, y, z, w):
        x, y, z, w = float(x), float(y), float(z), float(w)

        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll = math.atan2(t0, t1)
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch = math.asin(t2)
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw = math.atan2(t3, t4)
        return [roll, pitch, yaw]

    def euler_to_quaternion(self, roll, pitch, yaw):
        qx = math.sin(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) - math.cos(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
        qy = math.cos(roll/2) * math.sin(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.cos(pitch/2) * math.sin(yaw/2)
        qz = math.cos(roll/2) * math.cos(pitch/2) * math.sin(yaw/2) - math.sin(roll/2) * math.sin(pitch/2) * math.cos(yaw/2)
        qw = math.cos(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
        return [qx, qy, qz, qw]

    def __waypointHandler(self, data):
        self.waypointList.append(data)

    #add a list of points in ModelState
    def addPlanedPath(self, path):
        self.waypointList = path + self.waypointList

    def popNextPoint(self):
        if self.waypointList:
            return self.waypointList.pop(0)
        else:
            return None