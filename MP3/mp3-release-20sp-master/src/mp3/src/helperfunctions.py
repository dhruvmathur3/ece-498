import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np

# Possible Steering Angles
delta = [-50,-40,-30,-20,-10,0,10,20,30,40,50]

goal_Coord = [5,5]

chord_length = 1.43

###############################################################################
# Returns a list of two lists
# [0] - discrete nodes that can be accessed going forwards
# [1] - discrete nodes that can be accessed going backwards
###############################################################################
def possible_chord_paths(cont_x, cont_y, theta):

    # Forwards options
    forward_options = []

    for steering_angle in delta:

        if steering_angle == -50:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 50))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 50))),2)

        elif steering_angle == -40:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 40))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 40))),2)

        elif steering_angle == -30:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 30))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 30))),2)

        elif steering_angle == -20:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 20))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 20))),2)

        elif steering_angle == -10:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta - 10))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta - 10))),2)

        elif steering_angle == 0:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta))),2)

        elif steering_angle == 10:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 10))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 10))),2)

        elif steering_angle == 20:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 20))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 20))),2)
        
        elif steering_angle == 30:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 30))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 30))),2)

        elif steering_angle == 40:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 40))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 40))),2)

        elif steering_angle == 50:
            new_x = round(cont_x + (chord_length * math.cos(math.radians(theta + 50))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(theta + 50))),2)

        disc_cell = find_discrete_cell(new_x, new_y)
        forward_options.append(disc_cell)

    # Backwards options
    backwards_options = []

    for steering_angle in delta:

        if steering_angle == -50:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(50 + theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(50 + theta))),2)

        elif steering_angle == -40:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(40 + theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(40 + theta))),2)

        elif steering_angle == -30:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(30 + theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(30 + theta))),2)

        elif steering_angle == -20:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(20 + theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(20 + theta))),2)

        elif steering_angle == -10:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(10 + theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(10 + theta))),2)

        elif steering_angle == 0:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(theta))),2)
            new_y = round(cont_y - (chord_length * math.sin(math.radians(theta))),2)

        elif steering_angle == 10:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(10 - theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(10 - theta))),2)

        elif steering_angle == 20:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(20 - theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(20 - theta))),2)

        elif steering_angle == 30:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(30 - theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(30 - theta))),2)

        elif steering_angle == 40:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(40 - theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(40 - theta))),2)

        elif steering_angle == 50:
            new_x = round(cont_x - (chord_length * math.cos(math.radians(50 - theta))),2)
            new_y = round(cont_y + (chord_length * math.sin(math.radians(50 - theta))),2)

        disc_cell = find_discrete_cell(new_x, new_y) 
        backwards_options.append(disc_cell)      

    return forward_options,backwards_options

###############################################################################
# Finds which discrete node corresponds to the cont. coords
###############################################################################
def find_discrete_cell(cont_x, cont_y):

    return [round(cont_x),round(cont_y)]


###############################################################################
# Finds new state using equations from MP docs.
###############################################################################
def new_state(old_x, old_y, old_theta, velocity, steer_angle):

    new_Theta = round(old_theta + math.degrees(0.5 * math.tan(math.radians(steer_angle))))
    
    new_x = round(old_x + (velocity * math.cos(math.radians(new_Theta))),2)
    new_y = round(old_y + (velocity * math.sin(math.radians(new_Theta))),2)

    return new_Theta, new_x, new_y 

###############################################################################
# Returns a list of two lists
###############################################################################
def cost_to_goal(disc_x, disc_y):

    cost = round((goal_Coord[0] - disc_x)**2 + (goal_Coord[1] - disc_y)**2,2)

    return cost

###############################################################################
# Checks if cell is NN - use discrete coords
###############################################################################
def NN_check(orig_x, orig_y, next_x, next_y):

    if math.sqrt((next_x - orig_x)**2 + (next_y - orig_y)**2) > math.sqrt(2):
        return False
    
    return True


#--------------------------------------------------------------------------------------#
# For testing
#options = possible_chord_paths(-5, -5, 0)

#print(options[0])

#print(NN_check(-5,-5,-4,-3))