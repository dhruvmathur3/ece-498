# Plotting for MP4

import csv
import matplotlib.pyplot as plt
from math import *

pos_err_100_5 = []
head_err_100_5 = []
with open('100-500.0-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        pos_err_100_5.append(sqrt((float(row[0])-float(row[3]))**2 + (float(row[1])-float(row[4]))**2))
        head_err_100_5.append(abs(float(row[2])-float(row[5])))
pos_err_100_5 = pos_err_100_5[0:500]
head_err_100_5 = head_err_100_5[0:500]

pos_err_1000_2 = []
head_err_1000_2 = []
with open('1000-200.0-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        pos_err_1000_2.append(sqrt((float(row[0])-float(row[3]))**2 + (float(row[1])-float(row[4]))**2))
        head_err_1000_2.append(abs(float(row[2])-float(row[5])))
pos_err_1000_2 = pos_err_1000_2[0:500]
head_err_1000_2 = head_err_1000_2[0:500]

pos_err_1000_5 = []
head_err_1000_5 = []
with open('1000-500.0-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        pos_err_1000_5.append(sqrt((float(row[0])-float(row[3]))**2 + (float(row[1])-float(row[4]))**2))
        head_err_1000_5.append(abs(float(row[2])-float(row[5])))
pos_err_1000_5 = pos_err_1000_5[0:500]
head_err_1000_5 = head_err_1000_5[0:500]

pos_err_1000_10 = []
head_err_1000_10 = []
with open('1000-1000.0-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        pos_err_1000_10.append(sqrt((float(row[0])-float(row[3]))**2 + (float(row[1])-float(row[4]))**2))
        head_err_1000_10.append(abs(float(row[2])-float(row[5])))
pos_err_1000_10 = pos_err_1000_10[0:500]
head_err_1000_10 = head_err_1000_10[0:500]

pos_err_2500_5 = []
head_err_2500_5 = []
with open('2500-500.0-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        pos_err_2500_5.append(sqrt((float(row[0])-float(row[3]))**2 + (float(row[1])-float(row[4]))**2))
        head_err_2500_5.append(abs(float(row[2])-float(row[5])))
pos_err_2500_5 = pos_err_2500_5[0:500]
head_err_2500_5 = head_err_2500_5[0:500]

iters = [i for i in range(0, 500)]

plt.figure()
plt.plot(iters, pos_err_100_5, label = '100 particles')
plt.plot(iters, pos_err_1000_5, label = '1000 particles')
plt.plot(iters, pos_err_2500_5, label = '2500 particles')
plt.legend()
plt.title('Position Error')
plt.savefig('particlesvpos.png')

plt.figure()
plt.plot(iters, head_err_100_5, label = '100 particles')
plt.plot(iters, head_err_1000_5, label = '1000 particles')
plt.plot(iters, head_err_2500_5, label = '2500 particles')
plt.legend()
plt.title('Heading Error')
plt.savefig('particlesvhead.png')

plt.figure()
plt.plot(iters, pos_err_1000_2, label = 'Sensor quality 2')
plt.plot(iters, pos_err_1000_5, label = 'Sensor quality 5')
plt.plot(iters, pos_err_1000_10, label = 'Sensor quality 10')
plt.legend()
plt.title('Position Error')
plt.savefig('qualvpos.png')

plt.figure()
plt.plot(iters, head_err_1000_2, label = 'Sensor quality 2')
plt.plot(iters, head_err_1000_5, label = 'Sensor quality 5')
plt.plot(iters, head_err_1000_10, label = 'Sensor quality 10')
plt.legend()
plt.title('Heading Error')
plt.savefig('qualvhead.png')

plt.show()
