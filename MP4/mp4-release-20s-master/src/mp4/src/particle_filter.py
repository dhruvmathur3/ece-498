import numpy as np
from maze import Maze, Particle, Robot
import bisect
import rospy
from gazebo_msgs.msg import  ModelState
from gazebo_msgs.srv import GetModelState
import shutil
from std_msgs.msg import Float32MultiArray
from scipy.integrate import ode, odeint
import csv, sys

def func1(t, vars, vr, delta):
    curr_x = vars[0]
    curr_y = vars[1]
    curr_theta = vars[2]

    dx = vr * np.cos(curr_theta)
    dy = vr * np.sin(curr_theta)
    dtheta = delta
    return [dx,dy,dtheta]

class particleFilter:
    def __init__(self, gem, world, grid_width, grid_height, num_particles, sensor_limit, kernel_sigma, x_start, y_start):
        self.num_particles = num_particles  # The number of particles for the particle filter
        self.sensor_limit = sensor_limit    # The sensor limit of the sensor
        self.rand_particle_ratio = 0.1
        particles = list()
        gem.read_sensor()
        # print world.width, world.height
        for i in range(num_particles):
            x = np.random.uniform(0, world.width)
            # x = gem.x + np.random.normal(0,1000)
            y = np.random.uniform(0, world.height)
            # y = gem.y + np.random.normal(0,1000)
            particles.append(Particle(x = x, y = y, heading = gem.heading, maze = world, sensor_limit = sensor_limit))
        self.particles = particles          # Randomly assign particles at the begining
        self.bob = gem                      # The actual state of the vehicle
        self.world = world                  # The map of the maze
        self.grid_width = grid_width        # Each grid in gazebo simulator is divided into grid_width sections horizontally
        self.grid_height = grid_height      # Each grid in gazebo simulator is divided into grid_height sections vertically
        self.x_start = x_start              # The starting position of the map in the gazebo simulator
        self.y_start = y_start              # The starting position of the map in the gazebo simulator
        self.kernel_sigma = kernel_sigma
        self.modelStatePub = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=1)
        self.controlSub = rospy.Subscriber("/gem/control", Float32MultiArray, self.__controlHandler, queue_size = 1)
        self.control = []                   # A list of control signal from the vehicle
        return

    def __controlHandler(self,data):
        tmp = list(data.data)
        # print(tmp)
        self.control.append(tmp)

    def getModelState(self):
        """
        Description:
            Requests the current state of the polaris model when called
        Returns:
            modelState: contains the current model state of the polaris vehicle in gazebo
        """
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            serviceResponse = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            modelState = serviceResponse(model_name='polaris')
        except rospy.ServiceException as exc:
            rospy.loginfo("Service did not process request: "+str(exc))
        return modelState

    def weight_gaussian_kernel(self,x1, x2, std = 10):
        """
        Description:
            Given the sensor reading from vehicle and the sensor reading from particle, compute the weight of the particle based on gaussian kernel
        Input:
            x1: The sensor reading from vehicle
            x2: The sensor reading from particle
        Returns:
            Returns weight of the particle
        """
        distance = np.linalg.norm(np.asarray(x1) - np.asarray(x2))
        return np.exp(-distance ** 2 / (2 * std))

    def showMarker(self, x, y):
        """
        Description:
            Update the position of the marker in gazebo environment
        Input:
            x: x position of the marker
            y: y position of the marker
        Returns:
        """
        markerState = ModelState()
        markerState.model_name = 'marker'
        markerState.pose.position.x = x/self.grid_width + self.x_start - 100
        markerState.pose.position.y = y/self.grid_height + self.y_start - 100
        self.modelStatePub.publish(markerState)

    def quaternion_to_euler(self, x, y, z, w):
        """
        Description:
            converts quaternion angles to euler angles. Note: Gazebo reports angles in quaternion format
        Inputs:
            x,y,z,w:
                Quaternion orientation values
        Returns:
            List containing the conversion from quaternion to euler [roll, pitch, yaw]
        """
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll = np.arctan2(t0, t1)
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch = np.arcsin(t2)
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw = np.arctan2(t3, t4)
        return [roll, pitch, yaw]

    def updateWeight(self, readings_robot):

        ## TODO #####
        # Update the weight of each particle based on sensor reading from the robot
        sum_weights = 0.0
        for particle in self.particles:
            readings_particle = particle.read_sensor()
            ### DO I NEED TO UNROTATE THE READINGS??? I don't think so cause we should penalize particles with an incorrect heading
            weight = self.weight_gaussian_kernel(readings_robot, readings_particle) + 10**(-9) # or smaller
            sum_weights += weight
            particle.weight = weight

        # Normalize the weight
        # print(sum_weights)
        if sum_weights == 0.0:
            sum_weights = np.nextafter(0.0,1.0) # Avoid divide by 0 error
        for particle in self.particles:
            particle.weight /= sum_weights

        return

    def resampleParticle(self):

        particles_new = list()
        weights = list()
        for particle in self.particles:
            weights.append(particle.weight)
        resample_percent = 0.5
        true_resample = int(self.num_particles * resample_percent)
        random_resample = self.num_particles - true_resample

        ## TODO #####
        # Resample particles base on their calculated weight
        # Note: At least TWO different resampling method need to be implemented; You may add new function for other resampling techniques

        ###############
        # Method 1: Mulitnomial
        # """
        cumulative_sum = np.cumsum(weights)
        cumulative_sum[-1] = 1
        for i in range(true_resample):
            randomProb = np.random.uniform()
            index = np.searchsorted(cumulative_sum, randomProb)
            particles_new.append(Particle(x = self.particles[index].x, y = self.particles[index].y, heading = self.particles[index].heading, maze = self.world, sensor_limit = self.sensor_limit, noisy = True))

        for i in range(random_resample):
            index = np.random.randint(0,self.num_particles)
            particles_new.append(Particle(x = self.particles[index].x, y = self.particles[index].y, heading = self.particles[index].heading, maze = self.world, sensor_limit = self.sensor_limit, noisy = True))
        # """

        # Method 2: Roulette Wheel
        """
        index = int(np.random.random() * self.num_particles)
        beta = 0
        mw = np.max(weights)
        for i in range(true_resample):
            beta += np.random.random() * 2.0 * mw
            while beta > weights[index]:
                beta -= weights[index]
                index = (index + 1) % self.num_particles
            particles_new.append(Particle(x = self.particles[index].x, y = self.particles[index].y, heading = self.particles[index].heading, maze = self.world, sensor_limit = self.sensor_limit, noisy = True))

        for i in range(random_resample):
            index = np.random.randint(0,self.num_particles)
            particles_new.append(Particle(x = self.particles[index].x, y = self.particles[index].y, heading = self.particles[index].heading, maze = self.world, sensor_limit = self.sensor_limit, noisy = True))
        # """

        self.particles = particles_new

    def particleMotionModel(self):

        ## TODO #####
        # Update the position of each particle based on the control signal from the vehicle
        control = self.control
        self.control = []
        num_steps = len(control)
        if num_steps == 0:
            return

        y0, t0 = [-85.0, 45.0, 0.0], 0.0
        dt = 0.01
        r = ode(func1)
        r.set_initial_value(y0)
        steps = 0
        while r.successful() and steps < num_steps:
            r.set_f_params(control[steps][0], control[steps][1])
            r.integrate(r.t+dt)
            steps += 1
        x_diff = (r.y[0] + 85) * 100
        y_diff = (r.y[1] - 45) * 100
        heading_diff = r.y[2]
        vec_diff = np.sqrt(x_diff**2 + y_diff**2)
        vec_heading_diff = np.arctan2(y_diff, x_diff)
        # print("ODE: ", x_diff, y_diff, heading_diff)

        for particle in self.particles:
            # particle.x += x_diff*np.cos(particle.heading) - y_diff*np.sin(particle.heading)
            # particle.y += x_diff*np.sin(particle.heading) + y_diff*np.cos(particle.heading)
            particle.x += vec_diff * np.cos(particle.heading + vec_heading_diff)
            particle.y += vec_diff * np.sin(particle.heading + vec_heading_diff)
            particle.heading = (particle.heading + heading_diff) % (2*np.pi)
            particle.fix_invalid_particles() # Prevents particles from falling off the edge of the maze

        # for particle in self.particles:
        #     x = (particle.x / 100) - 85
        #     y = (particle.y / 100) + 45
        #     y0, t0 = [x, y, particle.heading], 0
        #     dt = 0.01
        #     print("before:",y0)
        #     r = ode(func1).set_initial_value(y0, t0)
        #     steps = 0
        #     while r.successful() and steps < num_steps:
        #         r.set_f_params(control[steps][0], control[steps][1])
        #         r.integrate(r.t+dt)
        #         steps += 1
        #     # r = ode(func1).set_initial_value(y0, t0).set_f_params(control[0][0], control[0][1])
        #     # steps = 1
        #     # while r.successful() and steps < num_steps:
        #     #     r.integrate(r.t+0.01)
        #     #     r = ode(func1).set_initial_value(r.y,r.t).set_f_params(control[steps][0], control[steps][1])
        #     #     steps += 1
        #     # r.integrate(r.t+0.01) # Finish integration on last control input
        #     print("after: ",r.y)
        #     particle.x = (r.y[0] + 85) * 100
        #     particle.y = (r.y[1] - 45) * 100
        #     particle.heading = r.y[2]
        #     particle.fix_invalid_particles() # Prevents particles from falling off the edge of the maze

        return

    def runFilter(self):
        # Run PF localization
        i = 0
        while True:
            ## TODO #####
            # Finish this function to have the particle filter running

            # Read sensor msg
            # Take a snapshot of the environment
            readings_robot = self.bob.read_sensor()
            curr_pos = [self.bob.x, self.bob.y, self.bob.heading]
            # if i != 0:
                # print("ROBOT: ", curr_pos[0] - prev_pos[0], curr_pos[1] - prev_pos[1], curr_pos[2] - prev_pos[2])
            # i = 1
            prev_pos = curr_pos
            self.particleMotionModel()
            self.updateWeight(readings_robot)

            # Display actual and estimated state of the vehicle and particles on map
            self.world.show_particles(particles = self.particles, show_frequency = 10)
            self.world.show_robot(robot = self.bob)
            # print(self.particles, "\n\n\n\n\n")
            #print(self.world.show_estimated_location(particles = self.particles))
            [est_x,est_y,est_head] = self.world.show_estimated_location(particles = self.particles)
            # print(est_x, est_y, "\n\n\n")
            self.world.clear_objects()
            self.showMarker(est_x, est_y)

            with open(str(self.num_particles)+"-"+str(self.sensor_limit)+'-data.csv', 'a') as fd:
                fd.write(str([self.bob.x, self.bob.y, self.bob.heading, est_x, est_y, est_head])+"\n")
            if i == 500:
                sys.exit()
            i += 1

            # Resample particles
            self.resampleParticle()
