# ECE 498 Safe Autonomy - Team 5

## Folders

The following is a table of contents for the folders:
- MP# - These are the folders corresponding to each MP
- Project - This folder contains the presentations and papers for the projects. All results from the tool simulations should be stored here.
- tool - This folder contains the tool for the project

## Project Details

### Requirements

Must have Python 3 installed, along with the following packages:
- numpy (pip)
- matplotlib (pip)
- pypoman (pip)
- polytope (pip)
- scipy (pip)
- [yices](https://github.com/SRI-CSL/yices2_python_bindings)

### Execution

From terminal, run `python3 PATH_TO_REPO/Project/Implementation/visualization.py`