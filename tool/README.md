# Tool for Implementation of Fast and Safe Controller Synthesis

## Description

This tool is to be implemented on the GEM car in High Bay at UIUC. There are several aspects to the implementation of this tool. There are several aspects to the implementation of this tool. These modules will be described below.

## Requirements

There are several requirements to implement this tool. These requirements are listed below.

- Python
- [Python Bindings for Yices 2](https://github.com/SRI-CSL/yices2_python_binding)
- numpy
- scipy
- [pypoman](https://pypi.org/project/pypoman/)
- [polytope](https://github.com/tulip-control/polytope)

Note that the SAT path planning module is currently implemented in Yices; it will soon be implemented using [Gurobi](https://www.gurobi.com/).

## Modules

The modules for this tool are as follows:

### Environment

This module is used to create an environment for the SAT path planner to solve. The environment should be easy to edit while a simulation is running. The environment module should be able to:
- Create an initial set from an estimated position
- Change location of goal and obstacles if needed

### SAT path planning

The path planning is implemented using a SAT solver. The SAT solver takes in an environment, maximum number of line segments, and a bloating factor. The environment consists of an initial set, goal set, and obstacle set. The bloating factor is used to bloat each obstacle in the obstacle set. The bloating factor is precomputed using a Lyapunov analysis. Each time a line segment is added, the obstacle set is bloated some more. From these inputs, the SAT path planner returns a list of nodes that corresponds to a path consisting of piecewise lines.

This module currently uses Yices, however it will be updated to Gurobi.

### Reference controller module

This modules is used to compute a reference controller from the paths given by the SAT path planner. The functions of this module are to:
- Create parametric line given nodes returned by SAT path planner
- Compute the reference state given a velocity mapping and a time stamp
- Compute the reference input given a velocity mapping and a time stamp

### Simulation module

This module is used to run simulations for the full algorithm. The car model used is a rearwheel bicycle model and the controller used is given in the pitch presentation. This controller can run the car both forwards and backwards.

### Algorithm module

This module contains the full algorithm for the implementation of the fast and safe controller synthesis. The algorithm is as follows:

1. Given an initial environment and velocity mapping, compute the initial reference controller
2. At each time step check the system state and environment state. If nothing has changed, continue using current reference controller. Recompute the reference controller if one of the following conditions has been satisfied:
	* The error from the current state to the reference state is greater than some threshhold.
	* The environment has changed.
	* A node has been reached.
3. If the controller must be recomputed, then recreate the initial set using the current estimated state and the sensor error. Ideally we should only recompute the parts of the controller that are no longer valid, however an initial naive approach is to recompute the entire controller.
4. If a controller cannot be computed or the goal set is reached, end the simulation.

