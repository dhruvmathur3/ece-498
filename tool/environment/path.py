# Path module for tool
# ECE 498
# Written by: Krisina Miller

# Path takes in a list of nodes, parametrizes the path
# Should be easy to get the reference states and inputs

import numpy as np
from numpy.linalg import norm
from scipy.misc import derivative

def create_path(nodes, T_tot):
	# Nodes are given as [[x_1, x_2, ...], ... ]
	dim = len(nodes[0])
	para_lists = [[float(node[j]) for node in nodes] for j in range(dim)]

	funcs = []
	for i in range(len(para_lists[0] - 1)):
		for j in range(dim):
			


	# # Calculate the length of the path
	# path_len = 0
	# leg_len = []
	# for i in range(len(x_list)-1):
	# 	p1 = np.array([x_list[i], y_list[i]])
	# 	p2 = np.array([x_list[i+1], y_list[i+1]])
	# 	leg_len.append(norm(p2-p1))
	# 	path_len += leg_len[-1]
	
	# # Create the piecewise function for the path
	# func = []
	# T = []
	# for i in range(len(x_list)-1):
	# 	T.append(T_tot*(leg_len[i])/path_len)

	# 	mx = (x_list[i+1] - x_list[i])
	# 	bx = x_list[i]
	# 	my = (y_list[i+1] - y_list[i])
	# 	by = y_list[i]
	# 	func.append(([mx, bx], [my, by]))

	# return func, T

nodes = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
T_tot = 1
print(create_path(nodes, T_tot))


def path(params, s):
	x_params = params[0]
	y_params = params[1]

	x = x_params[0]*s + x_params[1]
	y = y_params[0]*s + y_params[1]

	return np.array([x, y])


def trajectory(t, T, params):
	return path(params, t/T)

def tangent(idx, t, T, params):
	tan_ref = derivative(trajectory, t[idx], args = (T, params))
	tan_ref2 = derivative(trajectory, t[idx+1], args = (T, params))
	v_ref = norm(tan_ref)
	theta_ref = np.arctan2(tan_ref[1], tan_ref[0])
	theta_ref2 = np.arctan2(tan_ref2[1], tan_ref2[0])
	omega_ref = (theta_ref2 - theta_ref)/(t[idx+1] - t[idx])
	return [tan_ref, v_ref, theta_ref, omega_ref]