# Get the x_ref using yices
# Written by: Kristina Miller

import sys
sys.path.append("..")
from bloat_polytope import *
import numpy as np
import matplotlib.pyplot as plt
import pypoman as ppm
import polytope as pc
from yices import *

def add_initial_constraint(Theta):
	A0 = Theta[0]
	b0 = Theta[1]
	x_dim = len(A0[0])

	fmlas = []
	str0 = ''

	poly = pc.Polytope(A0, b0)
	pt = poly.chebXc

	for dim in range(x_dim):
		str0 = '(= x_%sref[0] %s)'%(dim+1, pt[dim])
		fmlas.append(str0)

	# for row in range(len(A0)):
	# 	b = b0[row][0]
	# 	for dim in range(x_dim):
	# 		a = A0[row][dim]
	# 		str1 = '(* %s x_%sref[0])'%(a, dim+1)
	# 		if dim == 0:
	# 			str0 = str1
	# 		else:
	# 			str0 = '(+ %s %s)'%(str0, str1)

	# 	fmla = '(<= %s %s)'%(str0, b)
	# 	fmlas.append(fmla)

	return fmlas

def add_final_constraint(Goal, N, alpha, k2):
	A1 = Goal[0]
	b1 = Goal[1]
	x_dim = len(A1[0])



	r = sqrt(alpha**2 + 4*(N - 1)/k2)
	b_new = bloat_polytope(A1, b1, -r)

	fmlas = []
	str0 = ''

	poly = pc.Polytope(A1, b1)
	pt = poly.chebXc

	for dim in range(x_dim):
		str0 = '(= x_%sref[%s] %s)'%(dim+1, N, pt[dim])
		fmlas.append(str0)

	# for row in range(len(A1)):
	# 	b = b_new[row][0]
	# 	for dim in range(x_dim):
	# 		a = A1[row][dim]
	# 		str1 = '(* %s x_%sref[%s])'%(a, dim+1, N)
	# 		if dim == 0:
	# 			str0 = str1
	# 		else:
	# 			str0 = '(+ %s %s)'%(str0, str1)

	# 	fmla = '(<= %s %s)'%(str0, b)
	# 	fmlas.append(fmla)

	return fmlas

def add_xref_constraints(O, N, alpha, k2):
	x_dim = len(O[0][0][0])

	str0 = ''
	str1 = ''
	fmlas = []
	for (Ao, bo) in O:
		for i in range(N):
			fmla = []
			for row in range(len(Ao)):
				if i == 0 or i == 1:
					r = alpha
				else:
					# r = sqrt(alpha**2 + 8*(i-1)/(k2*(1000 - 2)))
					# r = sqrt(alpha**2 + 4*(i-1)*(0.01 + 0.01 + 0.01))
					r = sqrt(alpha**2 + 4*(i-1)/k2)
					# r = alpha + 4*(i-1)/k2
				b_new = bloat_polytope(Ao, bo, r)
				
				b = b_new[row][0]
				for dim in range(x_dim):
					a = Ao[row][dim]
					str2 = '(* %s x_%sref[%s])'%(a, dim+1, i)
					str3 = '(* %s x_%sref[%s])'%(a, dim+1, i+1)
					if dim == 0:
						str0 = str2
						str1 = str3
					else:
						str0 = '(+ %s %s)'%(str0, str2)
						str1 = '(+ %s %s)'%(str1, str3)

				fmla0 = '(> %s %s)'%(str0, b)
				fmla1 = '(> %s %s)'%(str1, b)
				fmla2 = '(and %s %s)'%(fmla0, fmla1)
				fmla.append(fmla2)
			fmla4 = ' '.join(fmla)
			fmlas.append('(or '+fmla4+')')
	return fmlas

def add_search_constraints(search_area, N):
	A = search_area[0]
	b = search_area[1]
	x_dim = len(A[0])

	str0 = ''
	str1 = ''
	fmlas = []
	for i in range(N):
		fmla = []
		for row in range(len(A)):
			b1 = b[row][0]
			for dim in range(x_dim):
				a = A[row][dim]
				str0 = '(* %s x_%sref[%s])'%(a, dim+1, i)
				fmla0 = '(< %s %s)'%(str0, b1)
				fmla.append(fmla0)

			fmla4 = ' '.join(fmla)
			fmlas.append('(and '+fmla4+')')
	# print(fmlas[-1])
	return fmlas

def get_constraints(Theta, Goal, O, search_area, N, alpha, k2):
	ref_constraint = add_xref_constraints(O, N, alpha, k2)
	final_constraint = add_final_constraint(Goal, N, alpha, k2)
	initial_constraint = add_initial_constraint(Theta)
	# search_constraint = add_search_constraints(search_area, N)
	constraints = ref_constraint + final_constraint + initial_constraint
	 # + search_constraint

	return constraints

def get_xref_yices(Theta, Goal, O, search_area, N, alpha, k2, N_min = 1):
	# Theta is the initial set
	# Goal is the final set
	# O is a list of obstacles of the form Ax < b
	# N is the maximum number of line segments

	x_dim = len(Goal[0][0])

	cfg = Config()
	cfg.default_config_for_logic('QF_LRA')
	ctx = Context(cfg)

	real_t = Types.real_type()

	x_ref = [[Terms.new_uninterpreted_term(real_t, "x_%sref[0]" %(j+1)) for j in range(x_dim)], 
			 [Terms.new_uninterpreted_term(real_t, "x_%sref[1]" %(j+1)) for j in range(x_dim)]]
	# x_ref = [[Terms.new_uninterpreted_term(real_t, "x_%sref[%s]" %(j+1, i)) for j in range(x_dim)] for i in range(N+1)]

	for i in range(N_min, N):
		iters = i
		constraints = get_constraints(Theta, Goal, O, search_area, i, alpha, k2)
		fmlas = [Terms.parse_term(j) for j in constraints]

		ctx.assert_formulas(fmlas)

		status = ctx.check_context()
		if status == Status.SAT:
			# print('Path found, no more segments needed')
			n = i
			break
		else:
			# print('Adding new segment')
			ctx.dispose()
			ctx = Context(cfg)
			x_ref.append([Terms.new_uninterpreted_term(real_t, "x_%sref[%s]" %(j+1, i+1)) for j in range(x_dim)])

	x_path = []
	if status == Status.SAT:
		model = Model.from_context(ctx, 1)
		for i in range(n+1):
			x_new = []
			for j in range(x_dim):
				xval = model.get_value(x_ref[i][j])
				x_new.append(xval)
			x_path.append(x_new)
	else:
		x_path = None

	return x_path #, iters
	# return status == Status.SAT


def plot_path(x_path):
	x = list(zip(*x_path))[0]
	y = list(zip(*x_path))[1]
	plt.scatter(x, y)
	plt.plot(x, y)

if __name__ == '__main__':
	# The current scenario does not bloat the polytopes, but I will clean up the directory and add it to the bloating scenario
	A0 = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]])
	b0 = np.array([[0], [1], [0], [1]])
	Theta = (A0, b0)

	A1 = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]])
	b1 = np.array([[-4], [5], [-4], [5]])
	Goal = (A1, b1)

	A2 = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]])
	b2 = np.array([[-3], [3.5], [0], [5]])
	b3 = np.array([[-3], [7], [-5.5], [6]])
	b4 = np.array([[-3], [7], [1], [0]])
	O = [(A2, b2), (A2, b3), (A2, b4)]

	A = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]])
	b = np.array([[1], [15], [1], [15]])

	x_path = get_xref_yices(Theta, Goal, O, (A, b), 10, 0.1, 1000)

	ppm.polygon.plot_polygon(ppm.duality.compute_polytope_vertices(A0, b0), color = 'b')
	ppm.polygon.plot_polygon(ppm.duality.compute_polytope_vertices(A1, b1), color = 'g')
	for (A, b) in O:
		ppm.polygon.plot_polygon(ppm.duality.compute_polytope_vertices(A, b), color = 'r')

	for i in range(len(x_path)-1):
		plt.plot([x_path[i][0],x_path[i+1][0]], [x_path[i][1],x_path[i+1][1]])

	plt.xlim(-2, 20)
	plt.ylim(-2, 20)
	plt.grid()
	plt.show()